# Revision 2 (2023-05-17)

 - Fix Hi-Link AC/DC footprint
 - Correct DMP3099L pinout
 - Allow for THT and ST bulk capacitor
 - Revise layout for easier hand-soldering
 - Add additional 5V tracks for better current handling

# Revision 1 (2023-04-12)

 - Initial revision

